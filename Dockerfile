FROM alpine AS builder
ADD https://static.rust-lang.org/rustup/dist/x86_64-unknown-linux-musl/rustup-init /tmp/
ENV PATH=/root/.cargo/bin:${PATH}
COPY . /build/
RUN : \
 && apk update \
 && apk add --no-cache curl gcc musl-dev rust-stdlib \
 && chmod +x /tmp/rustup-init \
 && /tmp/rustup-init -y \
 && cd /build \
 && cargo build --release

FROM alpine
COPY --from=builder /build/target/release/roxyp /opt/
ENTRYPOINT ["/opt/roxyp", "-b", "0.0.0.0"]
