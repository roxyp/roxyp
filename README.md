# Roxyp

## Overview

Roxyp is an open source developer friendly http server.

It is written in Rust, scriptable in Typescript.

## Status

Under slow developement.
Not ready for production.

## Features

- Mock server
  - Serve JSON for API mocking purpose (stores mocks in file system)
  - Configurable status code and response time (simulate slow/buggy backends)
  - Add/update/delete endpoints via REST (maybe with an UI at some point)
- Reverse proxy
  - Retries (similar to Resilience4J)
  - Circuit breaking (similar to Resilience4J)
  - Http caching (store cache in file system, or Redis maybe)
  - Maybe rate limiting
- Type checked configuration (probably with Typescript)

## To Do

- Almost all the features listed above (this is a README driven development, see [here](https://tom.preston-werner.com/2010/08/23/readme-driven-development.html))

## Contributing

All contributions are welcome.
But I'd prefer advice over code right now.

## Goals

- Learn Rust
- Have fun
- Build a nice piece of tech
- Blog

## Inspirations

- [Nginx](https://nginx.org/)
- [Resilience4j](https://resilience4j.readme.io/)
- [K6](https://k6.io/)
- [Hyper](https://hyper.rs/)
- [WireMock](http://wiremock.org/)
- [Envoy](https://www.envoyproxy.io/)
- [Sozu](https://www.sozu.io/)
- More to come..

## Usage (this section should change at anytime)

### Mock server example

Create a directory "mock" with the following structure:

```
.
└── user
    ├── 1000
    │   └── data.json
    ├── 2000
    │   ├── data.json
    │   └── meta.yml
    ├── 3000
    │   ├── data.xml
    │   └── meta.yml
    └── 4000
        ├── data.json
        └── meta.yml
```

In the `data.json` files write some valid json.

In the `data.xml` file write some valid xml.

In the `2000/meta.yml` write the following:

```yml
status: 404
```

In the `3000/meta.yml` write the following:

```yml
ct: text/xml
```

In the `4000/meta.yml` write the following:

```yml
method: POST
status: 201
```

Run **roxyp** in the parent folder of the `mock` folder.

Call the following endpoints:

- `GET http://localhost:3000` should return the current configuration.
- `GET http://localhost:3000/user/1000` should return some json.
- `GET http://localhost:3000/user/2000` should return some json with status 404.
- `GET http://localhost:3000/user/3000` should return some xml.
- `POST http://localhost:3000/user/4000` should return some json with status 201.
