use hyper::{Body, Request, Response};
use std::time::{Duration, Instant};
use tokio::{fs, time};

use super::server;

mod meta;
mod path;
mod query;

use meta::Meta;

struct Mock {
    pub meta: Meta,
    pub data: Option<String>,
}

fn ct_to_ext(ct: &str) -> &str {
    match ct {
        "application/json" => ".json",
        "application/xml" | "text/xml" => ".xml",
        "application/javascript" => ".js",
        "text/html" => ".html",
        "text/css" => ".css",
        _ => "",
    }
}

async fn get_mock(folder: &str, p: &str) -> Option<Mock> {
    // Get mock path
    let mpath = path::get_path(folder, p)?;
    // Get meta
    let meta = fs::read_to_string(mpath.path.join("meta.yml")).await;
    let (meta, has_meta) = match meta {
        Ok(yaml) => (Meta::from_str(&yaml), true),
        Err(_) => (Meta::default(), false),
    };
    // Get data
    let data_path = mpath.path.join(format!("data{}", ct_to_ext(&meta.ct)));
    let data = match fs::read_to_string(data_path).await {
        Ok(s) => Some(mpath.vars.inject(s)),
        Err(_) => {
            if !has_meta {
                return None;
            }
            None
        }
    };
    Some(Mock { meta, data })
}

trait Vars {
    fn inject(&self, data: String) -> String;
}

pub async fn mock(req: &Request<Body>, start: &Instant, folder: &str) -> Option<Response<Body>> {
    let path = req.uri().path();
    let mock = get_mock(folder, &path[1..]).await?;
    if mock.meta.method != req.method() {
        return None;
    }
    let mut r = match mock.data {
        Some(s) => {
            let q = req.uri().query().map(query::parse);
            match q {
                Some(q) => Response::new(q.inject(s).into()),
                None => Response::new(s.into()),
            }
        }
        None => Response::new(Body::empty()),
    };
    *r.status_mut() = mock.meta.status;
    server::add_ct_header(&mut r, &mock.meta.ct);
    if let Some(time) = mock.meta.wait {
        if let Some(time) = Duration::from_millis(time).checked_sub(start.elapsed()) {
            time::sleep(time).await;
        }
    }
    Some(r)
}
