use hyper::header::{HeaderValue, SERVER};
use hyper::{Body, Response};

pub const VERSION: &str = env!("CARGO_PKG_VERSION");
pub const SERVER_NAME: &str = concat!("roxyp/", env!("CARGO_PKG_VERSION"));

const CT: &str = "Content-Type";
pub const JSON: &str = "application/json";

pub fn add_server_header(mut res: Response<Body>) -> Response<Body> {
    res.headers_mut()
        .insert(SERVER, HeaderValue::from_static(SERVER_NAME));
    res
}

pub fn add_ct_header(res: &mut Response<Body>, ct: &str) {
    res.headers_mut()
        .insert(CT, HeaderValue::from_str(ct).unwrap());
}

pub fn add_json_header(mut res: Response<Body>) -> Response<Body> {
    res.headers_mut().insert(CT, HeaderValue::from_static(JSON));
    res
}
