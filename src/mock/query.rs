enum Value<'a> {
    Simple(&'a str),
    None,
    Multiple(Vec<Option<&'a str>>),
}

impl<'a> Value<'a> {
    fn push(&mut self, val: Option<&'a str>) {
        match self {
            Value::Simple(s) => *self = Value::Multiple(vec![Some(s), val]),
            Value::None => *self = Value::Multiple(vec![None, val]),
            Value::Multiple(v) => v.push(val),
        }
    }
}

pub struct Query<'a>(Vec<(&'a str, Value<'a>)>);

pub fn parse(q: &str) -> Query {
    let mut query = Query(Vec::new());
    for item in q.split('&') {
        let pos = item.find('=');
        match pos {
            None => match query.0.iter_mut().find(|(k, _)| *k == item) {
                Some((_, v)) => v.push(None),
                None => query.0.push((item, Value::None)),
            },
            Some(pos) => match query.0.iter_mut().find(|(k, _)| *k == &item[..pos]) {
                Some((_, v)) => v.push(Some(&item[pos + 1..])),
                None => query
                    .0
                    .push((&item[..pos], Value::Simple(&item[pos + 1..]))),
            },
        };
    }
    query
}

impl<'a> super::Vars for Query<'a> {
    fn inject(&self, mut data: String) -> String {
        for (i, (k, v)) in self.0.iter().enumerate() {
            match v {
                Value::Simple(s) => {
                    data = data.replace(&format!("&{{{}}}", k), s);
                    data = data.replace(&format!("&{{{}[0]}}", k), s);
                    data = data.replace(&format!("?{{{}[0]}}", i), s);
                }
                Value::Multiple(v) => {
                    let mut last_val = "";
                    for (index, value) in v.iter().enumerate() {
                        if let Some(s) = value {
                            data = data.replace(&format!("&{{{}[{}]}}", k, index), s);
                            data = data.replace(&format!("?{{{}[{}]}}", i, index), s);
                            last_val = s;
                        }
                    }
                    data = data.replace(&format!("&{{{}}}", k), last_val);
                }
                _ => {}
            };
            data = data.replace(&format!("?{{{}}}", i), k);
        }
        data
    }
}
