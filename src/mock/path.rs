use std::path::{Path, PathBuf};

pub struct PathVars {
    values: Vec<String>,
}

impl super::Vars for PathVars {
    fn inject(&self, mut data: String) -> String {
        for (i, v) in self.values.iter().enumerate() {
            data = data.replace(&format!("${{{}}}", i), v);
        }
        data
    }
}

pub struct MockPath {
    pub path: PathBuf,
    pub vars: PathVars,
}

const WILDCARD: &str = "{}";

fn get_path_recursive(base: MockPath, url_path: &Path) -> Option<MockPath> {
    let path = base.path.join(url_path);
    if path.is_dir() {
        return Some(MockPath {
            path,
            vars: base.vars,
        });
    }
    // Search for wildcard folder
    let mut tail = PathBuf::new();
    let mut element = url_path.file_name()?;
    let mut parent = url_path.parent();
    while let Some(p) = parent {
        let path = base.path.join(p.join(WILDCARD));
        if path.is_dir() {
            let mut vars = base.vars;
            vars.values.push(element.to_str().unwrap().to_owned());
            return get_path_recursive(MockPath { path, vars }, &tail);
        }
        tail = Path::new(element).join(tail);
        element = p.file_name()?;
        parent = p.parent();
    }
    // No wildcard found
    None
}

pub fn get_path(folder: &str, url_path: &str) -> Option<MockPath> {
    get_path_recursive(
        MockPath {
            path: PathBuf::from(folder),
            vars: PathVars { values: Vec::new() },
        },
        Path::new(url_path),
    )
}
