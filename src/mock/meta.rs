use super::server;
use hyper::{Method, StatusCode};
use serde::Deserialize;

pub struct Meta {
    pub method: Method,
    pub status: StatusCode,
    pub ct: String,
    pub wait: Option<u64>,
}

#[derive(Deserialize)]
struct Yaml {
    method: Option<String>,
    status: Option<u16>,
    ct: Option<String>,
    wait: Option<u64>,
}

impl Meta {
    pub fn default() -> Meta {
        Meta {
            method: Method::GET,
            status: StatusCode::OK,
            ct: server::JSON.to_owned(),
            wait: None,
        }
    }

    pub fn from_str(yaml: &str) -> Meta {
        let yaml = serde_yaml::from_str::<Yaml>(yaml);
        let yaml = match yaml {
            Ok(s) => s,
            Err(e) => {
                panic!("Invalid yaml: {}", e);
            }
        };
        // Method
        let method = yaml.method;
        let method = match method {
            None => Method::GET,
            Some(m) => {
                let method: Result<Method, _> = m.to_uppercase().parse();
                match method {
                    Ok(m) => m,
                    Err(e) => {
                        panic!("Invalid method: {}", e);
                    }
                }
            }
        };
        // Status
        let status = yaml.status;
        let status = match status {
            None => StatusCode::OK,
            Some(s) => {
                let status = StatusCode::from_u16(s);
                match status {
                    Ok(s) => s,
                    Err(e) => {
                        panic!("Invalid status code: {}", e);
                    }
                }
            }
        };
        // Content type
        let ct = yaml.ct;
        let ct = match ct {
            None => server::JSON.to_owned(),
            Some(s) => s.to_lowercase(),
        };
        Meta {
            method,
            status,
            ct,
            wait: yaml.wait,
        }
    }
}
