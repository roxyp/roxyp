mod config;
mod log;
mod mock;
mod server;

pub use config::Config;
use config::Feature;
use hyper::{Body, Method, Request, Response, StatusCode};
use std::convert::Infallible;
use std::time::Instant;

fn info(config: &Config) -> Response<Body> {
    server::add_json_header(Response::new(
        format!(
            "{{\"version\":\"{}\",\"config\":{}}}",
            server::VERSION,
            serde_json::to_string(config).unwrap()
        )
        .into(),
    ))
}

fn not_found() -> Response<Body> {
    let mut r = Response::new(Body::empty());
    *r.status_mut() = StatusCode::NOT_FOUND;
    r
}

pub async fn serve(
    config: Config,
    start: Instant,
    req: Request<Body>,
) -> Result<Response<Body>, Infallible> {
    let r = if config.has_feature(Feature::Info)
        && req.uri().path() == config.info_endpoint()
        && req.method() == Method::GET
    {
        info(&config)
    } else if config.has_feature(Feature::Mock) {
        mock::mock(&req, &start, config.mock_folder())
            .await
            .unwrap_or_else(not_found)
    } else {
        not_found()
    };
    let r = server::add_server_header(r);
    log::req_res(&req, &r, &start);
    Ok(r)
}
