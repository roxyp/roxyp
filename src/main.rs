use hyper::service::{make_service_fn, service_fn};
use hyper::Server;
use std::convert::Infallible;
use std::time::Instant;

use roxyp::{serve, Config};

#[tokio::main]
async fn main() {
    let config = Config::new();
    let server = Server::bind(&config.addr())
        .serve(make_service_fn(|_| {
            let config = config.clone();
            async {
                Ok::<_, Infallible>(service_fn(move |req| {
                    let start = Instant::now();
                    serve(config.clone(), start, req)
                }))
            }
        }))
        .with_graceful_shutdown(shutdown_signal());

    match server.await {
        Err(e) => eprintln!("Server error: {}", e),
        Ok(_) => println!(" Bye!"),
    }
}

async fn shutdown_signal() {
    // Wait for the CTRL+C signal
    tokio::signal::ctrl_c()
        .await
        .expect("failed to install CTRL+C signal handler");
}
