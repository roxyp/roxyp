use hyper::{Body, Request, Response};
use std::time::Instant;

pub fn req_res(req: &Request<Body>, res: &Response<Body>, start: &Instant) {
    println!(
        "{} {} {} {}",
        req.method(),
        req.uri(),
        res.status().as_u16(),
        start.elapsed().as_micros()
    );
}
