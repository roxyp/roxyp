use std::env;
use std::env::VarError::{NotPresent, NotUnicode};
use std::fmt::Display;
use std::str::FromStr;

struct Arg {
    long: String,
    short: Option<String>,
}

pub struct Item {
    arg: Arg,
    var: String,
}

impl Item {
    pub fn new(name: &str, short_arg: Option<&str>) -> Item {
        let name = name.trim();
        if name.is_empty() {
            panic!("Config item name cannot be empty");
        };
        let mut long = String::from("--");
        long.push_str(&name.to_lowercase());
        let short = short_arg.map(|s| {
            let mut short = String::from("-");
            short.push_str(s);
            short
        });
        Item {
            arg: Arg { long, short },
            var: name.to_uppercase().replace(".", "_"),
        }
    }
}

// Find the value of an argument
fn get_arg_value(name: &Arg) -> Option<String> {
    let mut index = env::args().len();
    let long = format!("{}=", name.long);
    let short = match &name.short {
        Some(s) => s,
        None => "",
    };
    for (i, val) in env::args().enumerate() {
        if i == index + 1 {
            return Some(val);
        } else if val == name.long || val == short {
            index = i;
        } else if val.starts_with(&long) {
            return Some(String::from(&val[long.len()..]));
        }
    }
    None
}

// Get config from command line arguments
fn from_arg<T>(arg: &Arg) -> Option<T>
where
    T: FromStr,
    T::Err: Display,
{
    match get_arg_value(&arg) {
        Some(value) => match value.parse() {
            Ok(v) => Some(v),
            Err(e) => {
                eprintln!("Error parsing argument {}: {}", arg.long, e);
                None
            }
        },
        None => None,
    }
}

// Get config from environment variable
fn from_env<T>(key: &str) -> Option<T>
where
    T: FromStr,
    T::Err: Display,
{
    match env::var(key) {
        Ok(val) => match val.parse() {
            Ok(v) => Some(v),
            Err(e) => {
                eprintln!("Error parsing {} env var: {}", key, e);
                None
            }
        },
        Err(e) => match e {
            NotPresent => None,
            NotUnicode(_) => {
                eprintln!("Env var {} is not unicode: {}", key, e);
                None
            }
        },
    }
}

pub fn get_config_value<T>(item: Item) -> Option<T>
where
    T: FromStr,
    T::Err: Display,
{
    from_arg(&item.arg).or_else(|| from_env(&item.var))
}
