use super::get_config_value;
use serde::Serialize;

fn item(name: &str) -> super::Item {
    super::Item::new(&format!("info.{}", name), None)
}

#[derive(Clone, Serialize)]
pub struct Config {
    enabled: bool,
    endpoint: String,
}

impl Config {
    pub fn new() -> Self {
        Config {
            enabled: get_config_value(item("enabled")).unwrap_or(true),
            endpoint: get_config_value(item("endpoint")).unwrap_or_else(|| String::from("/")),
        }
    }

    pub fn is_ok(&self) -> bool {
        self.enabled && !self.endpoint.is_empty()
    }

    pub fn endpoint(&self) -> &str {
        &self.endpoint
    }
}
