use super::get_config_value;
use serde::Serialize;

fn item(name: &str) -> super::Item {
    super::Item::new(&format!("mock.{}", name), None)
}

#[derive(Clone, Serialize)]
pub struct Config {
    enabled: bool,
    folder: String,
}

impl Config {
    pub fn new() -> Self {
        Config {
            enabled: get_config_value(item("enabled")).unwrap_or(true),
            folder: get_config_value(item("folder")).unwrap_or_else(|| String::from("mock")),
        }
    }

    pub fn is_ok(&self) -> bool {
        self.enabled && !self.folder.is_empty()
    }

    pub fn folder(&self) -> &str {
        &self.folder
    }
}
