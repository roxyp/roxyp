mod info;
mod internal;
mod mock;

use internal::{get_config_value, Item};
use serde::Serialize;
use std::net::SocketAddr;

pub enum Feature {
    Mock,
    Info,
}

#[derive(Clone, Serialize)]
pub struct Config {
    port: u16,
    bind: String,
    info: info::Config,
    mock: mock::Config,
}

impl Config {
    pub fn new() -> Self {
        Config {
            port: get_config_value(Item::new("port", Some("p"))).unwrap_or(3000),
            bind: get_config_value(Item::new("bind", Some("b")))
                .unwrap_or_else(|| String::from("127.0.0.1")),
            info: info::Config::new(),
            mock: mock::Config::new(),
        }
    }

    pub fn addr(&self) -> SocketAddr {
        SocketAddr::new(self.bind.parse().unwrap(), self.port)
    }

    pub fn has_feature(&self, feat: Feature) -> bool {
        match feat {
            Feature::Mock => self.mock.is_ok(),
            Feature::Info => self.info.is_ok(),
        }
    }

    pub fn info_endpoint(&self) -> &str {
        self.info.endpoint()
    }

    pub fn mock_folder(&self) -> &str {
        self.mock.folder()
    }
}
